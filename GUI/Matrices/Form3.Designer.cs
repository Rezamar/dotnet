﻿namespace Matrices
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buscar = new System.Windows.Forms.Button();
            this.dataGridView01 = new System.Windows.Forms.DataGridView();
            this.InputB01 = new System.Windows.Forms.TextBox();
            this.init = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // buscar
            // 
            this.buscar.Location = new System.Drawing.Point(120, 63);
            this.buscar.Name = "buscar";
            this.buscar.Size = new System.Drawing.Size(75, 23);
            this.buscar.TabIndex = 0;
            this.buscar.Text = "Buscar";
            this.buscar.UseVisualStyleBackColor = true;
            this.buscar.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView01
            // 
            this.dataGridView01.AllowUserToAddRows = false;
            this.dataGridView01.AllowUserToDeleteRows = false;
            this.dataGridView01.AllowUserToResizeColumns = false;
            this.dataGridView01.AllowUserToResizeRows = false;
            this.dataGridView01.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView01.Location = new System.Drawing.Point(12, 106);
            this.dataGridView01.Name = "dataGridView01";
            this.dataGridView01.Size = new System.Drawing.Size(256, 192);
            this.dataGridView01.TabIndex = 1;
            this.dataGridView01.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dataGridView01_CellToolTipTextNeeded);
            this.dataGridView01.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView01_CellValueChanged);
            // 
            // InputB01
            // 
            this.InputB01.Location = new System.Drawing.Point(147, 37);
            this.InputB01.Name = "InputB01";
            this.InputB01.Size = new System.Drawing.Size(100, 20);
            this.InputB01.TabIndex = 3;
            // 
            // init
            // 
            this.init.Location = new System.Drawing.Point(39, 63);
            this.init.Name = "init";
            this.init.Size = new System.Drawing.Size(75, 23);
            this.init.TabIndex = 7;
            this.init.Text = "inicializar";
            this.init.UseVisualStyleBackColor = true;
            this.init.Click += new System.EventHandler(this.button3_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(21, 38);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 11;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(286, 310);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.init);
            this.Controls.Add(this.InputB01);
            this.Controls.Add(this.dataGridView01);
            this.Controls.Add(this.buscar);
            this.Name = "Form3";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buscar;
        private System.Windows.Forms.DataGridView dataGridView01;
        private System.Windows.Forms.TextBox InputB01;
        private System.Windows.Forms.Button init;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}