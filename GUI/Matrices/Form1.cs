﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrices
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();                      
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text.Equals("Multiplicación de Matrices")) MulMat();
            if (comboBox1.Text.Equals("Suma de Matrices")) SuMat();            
            if (comboBox1.Text.Equals("Busqueda Binaria")) BuBin();
            if (comboBox1.Text.Equals("Euclidez")) Euclidazo();
            if (comboBox1.Text.Equals("DMI (Inicial)")) Dmi();
        }

        void Dmi()
        {
            Form7 dmi = new Form7();
            dmi.Show();
        }

        void MulMat()
        {
            
        }

        void SuMat()
        {
            Form2 Form2 = new Form2();
            Form2.Show();

        }                
        void BuBin()
        {
            Form3 Form3 = new Form3();
            Form3.Show();

        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        void Euclidazo()
        {
            Form5 Form5 = new Form5();
            Form5.Show();
        }
    }
}